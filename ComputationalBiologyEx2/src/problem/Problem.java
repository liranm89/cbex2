package problem;
import java.util.ArrayList;
import mainPackage.Individual;

/**
 * Problem Super Class.
 * derived classes are different problems
 */
public abstract class Problem {
	protected int individualLength;
	/**
	 * Problem constructor
	 * @param individualLength the length of the individual
	 */
	public Problem (int individualLength) {
		this.individualLength = individualLength;
	}
	/**
	 * This method initializes the population
	 * @param population the population we want to initialize
	 * @param sizeOfPopulation the size of the wanted population
	 */
	public abstract void initialize(ArrayList<Individual> population, int sizeOfPopulation);
	
	/**
	 * This method initializes the database
	 * @param fileName name of the file
	 */
	public abstract void initializeDatabase(String fileName);
	
	/**
	 * this method calculates the fitness of an individual
	 * @param individual The individual
	 * @return its fitness
	 */
	public abstract double individualFitness(Individual individual);
	//
	/**
	 * this method calculates the fitness for all population
	 * @param population the population
	 */
	public void calculateFitness (ArrayList<Individual> population) {
		//For each individual
		for (int i = 0; i < population.size(); i++) {
			population.get(i).setScore(individualFitness(population.get(i)));
		}
	}
	public void learn(ArrayList<Individual> population) {
		for (int i = 0; i < population.size(); i++) {
			//For each individual
			//specific learn
			Individual trainedIndividual = this.individualLearn(population.get(i));
			trainedIndividual.setScore(individualFitness(trainedIndividual));
			//in case of a better result, update the individual
			if (trainedIndividual.getScore() >= population.get(i).getScore()) {
				population.set(i, trainedIndividual);
			} 
		}
	}
	protected abstract Individual individualLearn(Individual individual);
}
