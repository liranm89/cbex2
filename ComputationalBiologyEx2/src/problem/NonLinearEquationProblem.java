package problem;

import java.util.ArrayList;
import java.util.Random;

import mainPackage.Database;
import mainPackage.FileWriterAndReader;
import mainPackage.Individual;
import mainPackage.Main;
import mainPackage.PopulationSorter;
/**
 * Derived class of Problem,
 * Implements the non linear equations problem
 */
public class NonLinearEquationProblem extends Problem {
	private static final char MINUS_BIT = '1';
	private static final char PLUS_BIT = '0';
	private Database database = null;
	private ArrayList<String> options = new ArrayList<String>();
	
	/**
	 * Constructor
	 * @param individualLength the length of an individual
	 */
	public NonLinearEquationProblem(int individualLength) {
		super(individualLength);
		//We will use this for the learning type
			fillOptions("", Main.NUMBER_OF_VARIABLES);
	}
	
	/**
	 * This method initializes the population
	 * @param population the population we want to initialize
	 * @param sizeOfPopulation the size of the wanted population
	 */
	@Override
	public void initialize(ArrayList<Individual> population, int sizeOfPopulation) {
		Random rand = new Random();
		//For each individual
		for (int i = 0; i < sizeOfPopulation; i++) {
			String bitsString = "";
			//Create random bits
			for (int j = 0; j < this.individualLength; j++) {
				//Create random bits
				bitsString += Integer.toString(rand.nextInt(2));
			}
			Individual individual = new Individual(bitsString);
			population.add(individual);
		}

	}

	/**
	 * This method initializes the database
	 * @param fileName name of the file
	 */
	@Override
	public void initializeDatabase(String fileName) {
		try {
			this.database = new FileWriterAndReader().readFromFile(Main.FILENAME);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * this method calculates the fitness of an individual
	 * @param individual The individual
	 * @return its fitness
	 */
	@Override
	public double individualFitness(Individual individual) {
		//Get variables array
		int[] variables = getVariablesValues(individual);
		double answer = 0;
		double sum = 0;
		//for each equation
		for (int i = 0; i < Main.NUMBER_OF_EQUATIONS; i++) {
			//Get the value of the left side of the equation
			answer = getScoreFromEquation(i, variables);
			//Sum up the absolute value of the solution(right side) - the answer from above
			sum += Math.abs(this.database.getSolutions()[i] - answer);
		} 
		//If it's 0, we got the answer! Otherwise, 1/log(sum)
		//Reason for log: Because we want the fitness values to be closer to each other
		//Reason for 1/sum: We want the best individuals to have higher fitness
		//We need to add 1 because we do log on it, therefore this normalization
		sum += 1;
		return sum == 1 ? java.lang.Integer.MAX_VALUE : 1 / Math.log(sum);
	}

	/**
	 * this method gets the variables values from its bits
	 * @param individual The individual
	 * @return its variables values
	 */
	public static int[] getVariablesValues(Individual individual) {
		String DNA = individual.getDNA();
		int[] variables = new int[Main.NUMBER_OF_VARIABLES];
		int k = 0;
		//For each variable
		for (int i = 0; i < variables.length; i++) {
			//get its value from the bits
			variables[i] = bitsToInt(DNA.substring(k, k + Main.BITS_LENGTH));
			k += Main.BITS_LENGTH;
		}
		return variables;
	}
	
	/**
	 * This function gets the score from an equation
	 * @param i equation number
	 * @param variables the values of the variables
	 * @return
	 */
	public int getScoreFromEquation(int i, int[] variables) {
		int sum = 0;
		int multiplication = 1;
		int[][][] equations = this.database.getEquationsMatrix();
		//For each addition
		for (int j = 0; j < equations[i].length; j++) {
			//Multiply
			multiplication *= equations[i][j][0];
			//If it's 0, then just continue to the next multiplication
			if (multiplication == 0) {
				continue;
			}
			for (int k = 1; k < equations[i][j].length; k++) {
				//We want to avoid 0^0
				if (equations[i][j][k] != 0) {
					multiplication *=  Math.pow(variables[k - 1], equations[i][j][k]);
				}
			}
			sum += multiplication;
			multiplication = 1;
		}
		return sum;
	}

	/**
	 * @param bits
	 * @return base 10
	 */
	public static int bitsToInt(String bits) {
		int num = Integer.parseInt(bits.substring(1), 2);
		return bits.charAt(0) == MINUS_BIT ? -1 * num : num;
	}

	@Override
	protected Individual individualLearn(Individual individual) {
		
		int[] variables = getVariablesValues(individual);
		Random rand = new Random();
		
		ArrayList<Individual> listOfChangedIndividuals = new ArrayList<Individual>();

		int[][] variations = new int[this.options.size()][variables.length];
		for (int i = 0; i < variations.length; i++) {
			for (int j = 0; j < variations[i].length; j++) {
				if (this.options.get(i).charAt(j) == '1') {
					variations[i][j] = variables[j] + 1;
				} else  if (this.options.get(i).charAt(j) == '0'){
					variations[i][j] = variables[j] - 1;
				}
			}
		}
		
		for (int i = 0; i < variations.length; i++) {
			String bits = new String("");
			for (int j = 0; j < variations[i].length; j++) {
				if (variations[i][j] < 0) {
					bits += MINUS_BIT;
				} else {
					bits += PLUS_BIT;
				}
				bits += intToBits(variations[i][j], Main.BITS_LENGTH - 1);
			}
			Individual changedIndividual = new Individual(bits);
			changedIndividual.setScore(this.individualFitness(changedIndividual));
			listOfChangedIndividuals.add(changedIndividual);
		}
		listOfChangedIndividuals.sort(new PopulationSorter());
		return listOfChangedIndividuals.get(0);
	}
	
	/**
	 * fill options to get all variations (this is for the learning part)
	 * @param str current string
	 * @param length wanted length
	 */
	private void fillOptions(String str, int length) {
		if (str.length() == length) {
			this.options.add(str);
			return;
		} 
		fillOptions(str + "1",length);
		fillOptions(str + "0",length);
	}
	
	/**
	 * @param num number in base 10
	 * @param numOfBits number of bits wanted
	 * @return base 2 representation
	 */
	public static String intToBits(int num, int numOfBits) {
		String bits = "";
		for(int i = 0; i < numOfBits; i++) {
			if (num % 2 == 0) {
				bits = "0" + bits;
			} else {
				bits = "1" + bits;
			}
			num = num / 2;
		}

		return bits;
	}


}
