package geneticAlgorithms;

import java.util.ArrayList;

import crossover.Crossover;
import mainPackage.Individual;
import mainPackage.Main;
import mainPackage.PopulationSorter;
import mainPackage.RoundsAnalyzer;
import mainPackage.Selection;
import mutation.Mutation;
import parents.ParentsSelection;
import problem.Problem;
/**
 * This is the derived class of GeneticAlgorithm.
 * The implementation of this is very general, just the usual operators
 */
public class GeneralGeneticAlgorithm extends GeneticAlgorithm {
	
	/**
	 * GeneralGeneticAlgorithm constructor. Getting all necessary parameters
	 * @param problem The problem we want to solve
	 * @param populationSize The size of the population
	 * @param generations number of generations
	 * @param parentsSelection operation to select parents
	 * @param crossover crossover operation
	 * @param mutation mutation operation
	 * @param selection selection operation
	 * @param roundsAnalyzer to analyze all rounds
	 * @param crossoverRate crossover rate
	 * @param replicationRate replication rate
	 * @param main Main so we notify we finished
	 */
	public GeneralGeneticAlgorithm(Problem problem, int populationSize, int rounds, ParentsSelection parentsSelection,
			Crossover crossover, Mutation mutation, Selection selection, RoundsAnalyzer roundsAnalyzer,
			double crossoverRate, double replicationRate, Main main) {
		super(problem, populationSize, rounds, parentsSelection, crossover, mutation, selection, roundsAnalyzer,
				crossoverRate, replicationRate, main);
	}

	/**
	 * Make one generation
	 */
	@Override
	public void makeRound() {
		//Create offsprings
		ArrayList<Individual> offsprings = new ArrayList<Individual>();
		//From crossover
		for (int i = 0; i < Math.round(crossoverRate * this.selection.getSize()); i++) {
			//Choose parents. get their indexes
			ArrayList<Integer> parentsIndexes = this.parentsSelection.selectParents(this.population);
			ArrayList<Individual> parentsForCO = new ArrayList<Individual>();
			//Add them to a list
			parentsForCO.add(this.population.get(parentsIndexes.get(0)));
			parentsForCO.add(this.population.get(parentsIndexes.get(1)));
			//Create an hybrid offspring
			Individual offspring = this.crossover.makeCrossover(parentsForCO);
			//Add it to the offsprings list
			offsprings.add(offspring);
		}
		//Replication. Just replicate as it is.
		for (int i = 0 ; i < Math.round(replicationRate * this.selection.getSize()); i++) {
			//Biased selection
			ArrayList<Integer> index = this.parentsSelection.selectParents(this.population);
			offsprings.add(new Individual(this.population.get(index.get(0))));
		}
		//Mutation
		for (int i = 0 ; i < offsprings.size(); i++) {
			offsprings.set(i, this.mutation.mutate(offsprings.get(i)));
		}
		//change population
		this.selection.newPopulation(this.population, offsprings);
		this.problem.calculateFitness(this.population);
		//sort 
		population.sort(new PopulationSorter());
	}
}
