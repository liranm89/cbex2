package geneticAlgorithms;

import mutation.*;
import parents.*;
import problem.Problem;

import java.util.ArrayList;

import crossover.*;
import mainPackage.Individual;
import mainPackage.Main;
import mainPackage.RoundsAnalyzer;
import mainPackage.Selection;
/**
 * Super class of Genetic Algorithm
 */
public abstract class GeneticAlgorithm {
	protected Problem problem;
	protected ParentsSelection parentsSelection;
	protected Crossover crossover;
	protected Mutation mutation;
	protected Selection selection;
	protected int populationSize;
	protected ArrayList<Individual> population;
	protected int generations;
	protected RoundsAnalyzer roundsAnalyzer;
	protected double crossoverRate, replicationRate;
	protected Main main;
	
	/**
	 * GeneticAlgorithm constructor. Getting all necessary parameters
	 * @param problem The problem we want to solve
	 * @param populationSize The size of the population
	 * @param generations number of generations
	 * @param parentsSelection operation to select parents
	 * @param crossover crossover operation
	 * @param mutation mutation operation
	 * @param selection selection operation
	 * @param roundsAnalyzer to analyze all rounds
	 * @param crossoverRate crossover rate
	 * @param replicationRate replication rate
	 * @param main Main so we notify we finished
	 */
	public GeneticAlgorithm(Problem problem, int populationSize, int generations, 
			ParentsSelection parentsSelection, Crossover crossover, 
			Mutation mutation, Selection selection, RoundsAnalyzer roundsAnalyzer,
			double crossoverRate, double replicationRate, Main main) {
		this.problem = problem;
		this.populationSize = populationSize;
		this.generations = generations;
		this.parentsSelection = parentsSelection;
		this.crossover = crossover;
		this.mutation = mutation;
		this.selection =  selection;
		this.population = new ArrayList<Individual>();
		this.roundsAnalyzer = roundsAnalyzer;
		this.crossoverRate = crossoverRate;
		this.replicationRate = replicationRate;
		this.main = main;
	}
	
	/**
	 * Main method of the genetic algorithm. a loop to make the genetic algorithm
	 * @return best individual of the last generation
	 */
	public Individual startGA() {
		//Initialize population
		this.problem.initialize(this.population, this.populationSize);
		//Calculate population fitness
		this.problem.calculateFitness(this.population);
		int count = 1;
		while (count <= generations) {
			//Case you've got the answer - stop loop.
			if (this.population.get(0).getScore() == java.lang.Integer.MAX_VALUE) {
				solutionFoundNotification();
				break;
			}
			//Make one round
			this.makeRound();
			//Analyze it
			this.roundsAnalyzer.analyze(this.population, count);
			count++;
		}
		return this.population.get(0);
	}
	
	private void solutionFoundNotification() {
		this.main.solutionFound();
		
	}

	/**
	 * @return the population
	 */
	public ArrayList<Individual> getPopulation() {
		return this.population;
	}
	
	/**
	 * Make one generation
	 */
	public abstract void makeRound();
	
	/**
	 * Initialize population
	 */
	public void initialize() {
		this.population.clear();
	}
}
