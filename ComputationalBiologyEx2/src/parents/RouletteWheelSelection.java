package parents;

import java.util.ArrayList;
import java.util.Random;

import mainPackage.Individual;
/**
 * Derived class of ParentsSelection.
 * An implementation of Roulette Wheel Selection
 */
public class RouletteWheelSelection extends ParentsSelection{
	
	/**
	 * This method gets a population and selects parents from it
	 * @param population the population
	 * @return parents from it
	 */
	@Override
	public ArrayList<Integer> selectParents(ArrayList<Individual> population) {
		ArrayList<Integer> parents = new ArrayList<Integer>();
		//Make an accumulatedScore array list
		ArrayList<Double> accumulatedScore = new ArrayList<Double>();
		double sumOfScore = 0;
		//Calculate accumulated score
		for (int i = 0; i < population.size(); i++) {
			sumOfScore += population.get(i).getScore();
			accumulatedScore.add(sumOfScore);
		}
		int parent1 = chooseParent(accumulatedScore, sumOfScore);
		int parent2 = chooseParent(accumulatedScore, sumOfScore);
		if (parent1 == parent2) {
			parent2 = (parent1 + 1) % population.size();
		}
		parents.add(parent1);
		parents.add(parent2);
		return parents;
	}
	
	/**
	 * biased parent selection
	 * @param accumulatedScore the accumulated score of the population
	 * @param sumOfScore the total sum
	 * @return the index of the biased parent
	 */
	private static int chooseParent(ArrayList<Double> accumulatedScore, double sumOfScore) {
		Random rand = new Random();
		int parent = 0;
		//Initialize dice1 from 0 to the sum of score
		double dice = rand.nextDouble() * sumOfScore;
		for (int i = 0; i < accumulatedScore.size(); i++) {
			//If smaller than choose the parent
			if (dice <= accumulatedScore.get(i)) {
				parent = i;
				break;
			}
		}
		return parent;
	}
}
