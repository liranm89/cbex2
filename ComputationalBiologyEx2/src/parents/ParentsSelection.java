package parents;

import java.util.ArrayList;

import mainPackage.Individual;
/**
 * This class is the super class, ParentsSelection class
 * Its responsible is to choose parents, in any method
 */
public abstract class ParentsSelection {
	
	/**
	 * This method gets a population and selects parents from it
	 * @param population the population
	 * @return parents from it
	 */
	public abstract ArrayList<Integer> selectParents(ArrayList<Individual> population);
	
	/**
	 * For personal use
	 */
	public String toString() {
		return this.getClass().getSimpleName();
	}
}