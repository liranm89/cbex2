package mutation;

import java.util.ArrayList;
import java.util.Random;

import mainPackage.Individual;
/**
 * Super class of Mutation. All derived classes can implements a different mutation
 */
public abstract class Mutation {
	protected double mutationRate;
	/**
	 * Mutation constructor
	 * @param mutationRate the mutation rate
	 */
	public Mutation(double mutationRate) {
		this.mutationRate = mutationRate;
	}

	/**
	 * This method tries to mutate an individual
	 * @param offspring the individual we want to mutate
	 * @return the new mutated individual - IF it mutated
	 */
	public Individual mutate(Individual offspring) {
		//Make a random number
		Random rand = new Random();
		//Create a new individual
		Individual individual = new Individual(offspring);
		//If it's less than the mutation rate, then mutate
		if (rand.nextDouble() <= this.mutationRate) {
			individual.setDNA(this.mutateDNA(individual.getDNA()));
		}
		return individual;
	}
	
	/**
	 * Mutates the Individual
	 * @param mutatedOffspring A string we want to mutate
	 * @return the new, mutated string
	 */
	public abstract String mutateDNA(String mutatedOffspring);
	
	/**
	 * @return the mutation rate
	 */
	public double getMutationRate() {
		return this.mutationRate;
	}
	
	/**
	 * For personal use.
	 */
	public String toString() {
		return this.getClass().getSimpleName() + "\t With rate: " + this.mutationRate;
	}
	
	/**
	 * This function chooses a subset of numbers.
	 * 
	 * @param limit The maximum number (excluded)
	 * @param minimum The minimum number (included)
	 * @param subsetSize The size of the subset
	 * @return An Integer ArrayList of the subset
	 */
	public static ArrayList<Integer> chooseSubset(int limit, int minimum, int subsetSize) {
		//Initialize a random variable
		Random rand = new Random();
		//Subset of indexes to scramble
		ArrayList<Integer> subsetToScramble = new ArrayList<Integer>();
		//We need a list of numbers from 0 to limit - 1, so we can choose from them randomly, efficiently. 
		int listOfNumbers[] = new int[limit];
		//Fill listOfNumbers
		for (int i = 0; i < limit; i++) {
			listOfNumbers[i] = i;
		}
		//Do this the subset size times - means every time a random index will be inserted to subsetToScramble
		for (int i = 0; i < subsetSize; i++) {
			//Get a random index
			int randomIndex = rand.nextInt(limit - minimum) + minimum;
			//Add the number at the random index
			subsetToScramble.add(listOfNumbers[randomIndex]);
			//We don't want to encounter this number again, therefore we switch
			//Between the number in the random index to the last index
			listOfNumbers[randomIndex] = listOfNumbers[limit - 1];
			//Now we will reduce limit to limit - 1.
			limit--;
		}
		return subsetToScramble;
	}
}
