package mutation;

import java.util.Random;
/**
 * Derived class of Mutation. A simple bit flip implementation
 */
public class BitFlipMutation extends Mutation {
	
	/**
	 * BitFlipMutation constructor
	 * @param mutationRate the mutation rate
	 */
	public BitFlipMutation(double mutationRate) {
		super(mutationRate);
	}

	/**
	 * Mutates the Individual
	 * @param mutatedOffspring A string we want to mutate
	 * @return the new, mutated string
	 */
	@Override
	public String mutateDNA(String mutatedOffspring) {
		//Get a random number
		Random rand = new Random();
		int index = rand.nextInt(mutatedOffspring.length());
		//copy to char array in order to change
		char[] dnaCharArray = mutatedOffspring.toCharArray();
		if (dnaCharArray[index] == '0') {
			dnaCharArray[index] = '1';
		} else {
			dnaCharArray[index] = '0';
		}
		return String.valueOf(dnaCharArray);
	}

}
