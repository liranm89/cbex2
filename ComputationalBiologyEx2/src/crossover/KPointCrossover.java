package crossover;

import java.util.ArrayList;
import java.util.Random;

import mainPackage.Individual;
import mutation.Mutation;
/**
 * this is the derived class of Crossover which makes crossover in K points
 */
public class KPointCrossover extends Crossover {
	
	/**
	 * KPointCrossover constructor
	 * @param numberOfCrossovers number of crossovers wanted (K)
	 */
	public KPointCrossover(int numberOfCrossovers) {
		super(numberOfCrossovers);
	}
	
	/**
	 * makes crossover from N parents
	 * @param parents chosen parents to make crossover
	 * @return new individual
	 */
	@Override
	public Individual makeCrossover(ArrayList<Individual> parents) {
		//Get the individual length
		int individualLength = parents.get(0).getDNA().length();
		Random rand = new Random();
		//Select a crossover point. from 1(included) to length - 1 (included)
		//No option to choose crossover point at index 0, because no crossover will happen this way.
		ArrayList<Integer> crossoverPoints = Mutation.chooseSubset(individualLength, 1, this.numberOfCrossovers);
		crossoverPoints.sort(new IntegerSorter());
		int crossoverPointIndex = 0;
		//Select a random index from the parents. e.g.: 0 or 1
		int indexOfSelectedString = rand.nextInt(parents.size());
		String selectedString = parents.get(indexOfSelectedString).getDNA();
		String newString = "";
		//Select a parent string
		for (int i = 0; i < individualLength; i++) {
			//If it's the crossover point, then change the selectedString (change the parent)
			if (i == crossoverPoints.get(crossoverPointIndex)) {
				selectedString = parents.get((indexOfSelectedString + 1) % parents.size()).getDNA();
				indexOfSelectedString++;
				crossoverPointIndex = (crossoverPointIndex + 1) % crossoverPoints.size();
			}
			//Copy the char from the selectedString to newString
			newString += selectedString.charAt(i);
		}
		Individual newIndividual = new Individual(newString);
		return newIndividual;
	}
}


