package crossover;

import java.util.ArrayList;
import java.util.Comparator;

import mainPackage.Individual;
/**
 * This is the super class for the Crossover operation
 */
public abstract class Crossover {
	protected int numberOfCrossovers;
	/**
	 * Crossover constructor
	 * @param numberOfCrossovers number of crossovers
	 */
	public Crossover(int numberOfCrossovers) {
		this.setNumberOfCrossovers(numberOfCrossovers);
	}
	
	/**
	 * makes crossover from N parents
	 * @param parents chosen parents to make crossover
	 * @return new individual
	 */
	public abstract Individual makeCrossover(ArrayList<Individual> parents);
	
	/**
	 * @return number of crossovers
	 */
	public int getNumberOfCrossovers() {
		return numberOfCrossovers;
	}
	/**
	 * @param numberOfCrossovers number of crossovers
	 */
	public void setNumberOfCrossovers(int numberOfCrossovers) {
		this.numberOfCrossovers = numberOfCrossovers;
	}
	
	/**
	 * for personal use
	 */
	public String toString() {
		return this.getClass().getSimpleName() + "\t with crossovers #: " + this.numberOfCrossovers;
	}
	
	/**
	 * Just to sort integers
	 */
	protected class IntegerSorter implements Comparator<Integer> {
		@Override
		public int compare(Integer arg0, Integer arg1) {
			return arg0 > arg1 ? 1 : arg0 == arg1 ? 0 : -1;
		}
	}
}
