package mainPackage;

import java.util.ArrayList;
import crossover.Crossover;
import crossover.KPointCrossover;
import geneticAlgorithms.GeneralGeneticAlgorithm;
import geneticAlgorithms.GeneticAlgorithm;
import geneticAlgorithms.LamarckianGeneticAlgorithm;
import mutation.BitFlipMutation;
import mutation.Mutation;
import parents.ParentsSelection;
import parents.RouletteWheelSelection;
import problem.Problem;
import problem.NonLinearEquationProblem;
/**
 * Tamar Wasserman			Liran Matatof
 * ID: 305179202			201015724
 * 
 * Main class
 */
public class Main {
	public static String FILENAME, OUTPUT_FILE = "results.csv";
	//Genetic Algorithm Parameters
	public static final int NUMBER_OF_MULTIPLICATIONS = 5;
	public static int NUMBER_OF_EQUATIONS, NUMBER_OF_VARIABLES;
	public static final int BITS_LENGTH = 6;
	public static final int CROSSOVER_POINTS = 1, POPULATION_SIZE = 100, 
			REPLACE = POPULATION_SIZE - 1, GENERATIONS = 100;
	public static final double MUTATION_RATE = 0.2, CROSSOVER_RATE = 1,
			REPLICATION_RATE = 1 - CROSSOVER_RATE;
	private Crossover crossover;
	private Mutation mutation;
	private ParentsSelection parentsSelection;
	private Selection selection;
	private Problem problem;
	private GeneticAlgorithm geneticAlgorithm;
	private RoundsAnalyzer roundsAnalyzer;
	private FileWriterAndReader fileWriterAndReader;
	private Boolean foundAnswer = false;

	/**
	 * Main constructor. Initialize the wanted operations
	 */
	public Main() { 
		//Create wanted operations
		this.crossover = new KPointCrossover(CROSSOVER_POINTS);
		this.parentsSelection = new RouletteWheelSelection();
		this.selection = new Selection(REPLACE);
		this.problem = new NonLinearEquationProblem(NUMBER_OF_VARIABLES * BITS_LENGTH);
		this.mutation = new BitFlipMutation(MUTATION_RATE);
		this.roundsAnalyzer = new RoundsAnalyzer();
		this.fileWriterAndReader = new FileWriterAndReader();
	}  
	
	/**
	 * @param args arguments from the user
	 */
	public static void main(String[] args) {
		Main.FILENAME = args[0];
		Main.NUMBER_OF_VARIABLES = Integer.parseInt(args[1]);
		Main.NUMBER_OF_EQUATIONS = NUMBER_OF_VARIABLES;
		Main main = new Main();
		main.solve();
	}
	
	/**
	 * This method is solving the problem
	 */
	private void solve() {
		this.problem = new NonLinearEquationProblem(NUMBER_OF_VARIABLES * BITS_LENGTH);
		this.problem.initializeDatabase(FILENAME);
		ArrayList<Individual> results = new ArrayList<Individual>();
		//try 10 runs, but if it finishes before - then show on the screen,
		//Otherwise it didn't find any solution
		for (int i = 0; i < 10; i++) {
			this.geneticAlgorithm = new LamarckianGeneticAlgorithm(this.problem, POPULATION_SIZE, GENERATIONS,
					this.parentsSelection, this.crossover, this.mutation, this.selection, this.roundsAnalyzer,
					CROSSOVER_RATE, REPLICATION_RATE, this);
			Individual individual = this.geneticAlgorithm.startGA();
			results.add(individual);
			if (this.foundAnswer) {
				this.fileWriterAndReader.writeToFile(OUTPUT_FILE, this.roundsAnalyzer.toString());
				break;
			}
			this.roundsAnalyzer.initialize();
		}
		results.sort(new PopulationSorter());
		System.out.println(results.get(0));
	}
	
	/**
	 * Event handler if a solution was found
	 */
	public void solutionFound() {
		this.foundAnswer = true;
	}
	

}
