package mainPackage;

import java.util.ArrayList;

/**
 * RoundsAnalyzer class. It analyzes the data of each generation
 */
public class RoundsAnalyzer {
	private ArrayList<Double> max;
	private ArrayList<Double> min;
	private ArrayList<Double> average;
	private ArrayList<Integer> round;
	
	/**
	 * RoundsAnalyzer constructor
	 */
	public RoundsAnalyzer() {
		//initialize all the lists
		this.max = new ArrayList<Double>();
		this.min = new ArrayList<Double>();
		this.average = new ArrayList<Double>();
		this.round = new ArrayList<Integer>();
	}
	
	/**
	 * @return the maximum list
	 */
	public ArrayList<Double> getMax() {
		return this.max;
	}
	/**
	 * @return the minimum list
	 */
	public ArrayList<Double> getMin() {
		return this.min;
	}
	/**
	 * @return the average list
	 */
	public ArrayList<Double> getAverage() {
		return this.average;
	}
	
	/**
	 * This method analyzes each generation
	 * @param population The population at this generation
	 * @param roundNumber round/generation number
	 */
	public void analyze(ArrayList<Individual> population, int roundNumber) {
		this.round.add(roundNumber);
		this.max.add(population.get(0).getScore());
		this.min.add(population.get(population.size() - 1).getScore());
		findAverage(population);

	}
	
	/**
	 * finds the average fitness of the population
	 * @param population the population
	 */
	private void findAverage(ArrayList<Individual> population) {
		double sum = 0;
		for (Individual individual: population) {
			sum += individual.getScore();
		}
		this.average.add(sum / population.size());
	}
	
	/**
	 * initializes all the lists of the RoundsAnalyzer
	 */
	public void initialize() {
		this.round.clear();
		this.min.clear();
		this.max.clear();
		this.average.clear();
	}
	
	/**
	 * For personal use
	 */
	public String toString() {
		String str = "";
		str = "Round, Maximum, Average, Minimum\n";
		for (int i = 0; i < this.round.size(); i++) {
			str = str + this.round.get(i) + "," + this.max.get(i) + "," + 
					this.average.get(i) + "," + this.min.get(i) + "\n";
		}
		return str;
	}

}
