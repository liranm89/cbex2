package mainPackage;

import problem.NonLinearEquationProblem;
/**
 * This is the individual class. 
 */
public class Individual {
	private String DNA;
	private double score;
	/**
	 * Individual constructor
	 * @param DNA The DNA of the individual
	 */
	public Individual(String DNA) {
		setDNA(DNA);
	}
	/**
	 * Copy constructor
	 * @param individual an Individual which we want to copy from
	 */
	public Individual(Individual individual) {
		this.DNA = new String(individual.DNA);
		this.score = individual.score;
	}
	
	/**
	 * @param DNA The DNA of the individual
	 */
	public void setDNA(String DNA) {
		this.DNA = DNA;
	}
	
	/**
	 * @param score The fitness of the individual
	 */
	public void setScore(double score) {
		this.score = score;
	}
	
	/**
	 * @return the DNA
	 */
	public String getDNA() {
		return this.DNA;
	}
	
	/**
	 * @return its score (fitness)
	 */
	public double getScore() {
		return this.score;
	}
	
	/**
	 * For personal use
	 */
	public String toString() {
		int[] variables = NonLinearEquationProblem.getVariablesValues(this);
		String solution = "[";
		for (int i = 0; i < variables.length; i++) {
			solution += variables[i] + ",";
		}
		solution = solution.substring(0, solution.length() - 1);
		solution += "]";
		return "DNA:\t" + this.DNA + "\tScore:\t" + this.score + "\n" + solution;
	}
}
