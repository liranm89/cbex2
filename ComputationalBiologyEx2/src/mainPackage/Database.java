package mainPackage;
/**
 * In this class we save the data from the file
 */
public class Database {
	private int[][][] equationsMatrix;
	private int[] solutions;
	/**
	 * Database constructors. 
	 * @param equationsMatrix 3d equations matrix
	 * @param solutions solutions vector
	 */
	public Database(int[][][] equationsMatrix, int[] solutions) {
		setEquationsMatrix(equationsMatrix);
		setSolutions(solutions);
	}
	/**
	 * @return the equations matrix
	 */
	public int[][][] getEquationsMatrix() {
		return equationsMatrix;
	}
	/**
	 * @param equationsMatrix the equations matrix
	 */
	public void setEquationsMatrix(int[][][] equationsMatrix) {
		this.equationsMatrix = equationsMatrix;
	}
	/**
	 * @return The solutions vector
	 */
	public int[] getSolutions() {
		return solutions;
	}
	/**
	 * @param solutions solutions vector
	 */
	public void setSolutions(int[] solutions) {
		this.solutions = solutions;
	}
	
	/**
	 * For personal use
	 */
	public String toString() {
		String str = "";
		for (int i = 0; i < equationsMatrix.length; i++) {
			for (int j = 0; j < equationsMatrix[i].length; j++) {
				for (int k = 0; k < equationsMatrix[i][j].length; k++) {
					if (k == 0) {
						str += equationsMatrix[i][j][k];
					} else {
						str += "," + equationsMatrix[i][j][k];
					}
				}
				str += "\n";
			}
			str += this.solutions[i] + "\n";
		}
		return str;
	}

}
