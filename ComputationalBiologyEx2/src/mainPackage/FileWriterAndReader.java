package mainPackage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
/**
 * This class handles files - reading from a file and writing a file
 */
public class FileWriterAndReader {
	private static final int SOLUTION_LINE = 6;
	private PrintWriter pw;
	/**
	 * Writing to a file a given string
	 * @param fileName destination file
	 * @param stringToWrite input string
	 */
	public void writeToFile(String fileName, String stringToWrite) {
		try {
			this.pw = new PrintWriter(new File(fileName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		this.pw.write(stringToWrite);
		this.pw.close();
	}
	
	/**
	 * This method reads a file
	 * @param fileName the name of the file
	 * @return the data from the csv
	 * @throws Exception if there was a problem with the file
	 */
	public Database readFromFile(String fileName) throws Exception {
		FileInputStream	fileInputStream = new FileInputStream(new File(fileName));
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
		String line = null;

		int[][][] equationsMatrix = new int[Main.NUMBER_OF_EQUATIONS]
				[Main.NUMBER_OF_MULTIPLICATIONS][Main.NUMBER_OF_VARIABLES + 1];
		int[] solutions = new int[Main.NUMBER_OF_EQUATIONS];
		Database database = new Database(equationsMatrix, solutions);
		int count = 1, k = 0;
		//read each line
		while ((line = bufferedReader.readLine()) != null) {
			//solution line
			if (count % SOLUTION_LINE == 0) {
				//When edited the file I saw "," on the solution line, so just in case, to avoid it.
				if (line.contains(",")) {
					line = line.substring(0, line.indexOf(','));
				}
				//Parse the string to integer
				solutions[k] = Integer.parseInt(line.toString());
				k++;
				count = 0;
				if (k == Main.NUMBER_OF_EQUATIONS) break;
			} else {
				//equation fix
				if (!line.startsWith("0")) {
					//split by ','
					String[] elements = line.split(",");
					//save the data
					for (int i = 0; i < Main.NUMBER_OF_VARIABLES + 1; i++) {
						equationsMatrix[k][count - 1][i] = Integer.parseInt(elements[i]);
					}
				}
			}
			count++;
		}
		bufferedReader.close();
		return database;
	}
}
